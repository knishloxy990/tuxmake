#!/bin/sh

set -eu

cd $(readlink -f $(dirname $0)/..)

current_version=$(python3 -c "import ${PROJECT}; print(${PROJECT}.__version__)")
version=${1:-}
if [ -z "${version}" ]; then
    version=$(echo ${current_version} | awk -F . '{OFS = "." ; $3 = "0"; $2 += 1; print($0)}')
fi
relnotes=.git/relnotes-${version}.txt
git=${git:-git}
flit=${flit:-flit}

if [ -n "$(git tag --list v${version})" ]; then
    echo "Version ${version} already released. Bump the version in ${PROJECT}/__init__.py to make a new release"
    exit 1
fi

if ! git diff-index --exit-code --quiet HEAD; then
    git status
    echo "Commit all changes before releasing"
    exit 1
fi

if [ ! -f ${relnotes} ]; then
    printf "${version} release\n\n" > ${relnotes}
    printf "New features:\n\n" >> ${relnotes}
    printf "Bug fixes:\n\n" >> ${relnotes}
    printf "Documentation updates:\n\n" >> ${relnotes}
    printf "Container images:\n\n" >> ${relnotes}
    (git log --no-merges --reverse --oneline v${current_version}.. || true) >> ${relnotes}
fi

${EDITOR} ${relnotes}
echo "Release notes: "
sed -e 's/^/| /' ${relnotes}

read -p "Press ENTER to release version ${version} with the release notes above, or ctrl-c to abort" input

# update version number
sed -i -e "s/^__version__.*/__version__ = \"${version}\"/" ${PROJECT}/__init__.py
if [ -f "${PROJECT}.spec" ]; then
    sed -i -e "s/^Version:.*/Version:   ${version}/" ${PROJECT}.spec
fi
if [ -f debian/changelog ]; then
    sed -i -e "1 { s/(.*)/(${version}-1)/ }" debian/changelog
fi
$git commit -a --file=${relnotes}

$git push
$git tag --sign --message="${version} release" v${version}
$flit publish
$git push --tags
rm -f ${relnotes}
